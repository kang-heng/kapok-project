// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    organization_name: ['中华支教与助学信息中心','中国旅行义工支教组织','中国大学生支教联盟','中华志愿者协会','好友营支教','中国青年志愿者协会','为中国而教'],
    organization_intro: ['以义工精神为原动力','用我所能助教山区','改变贫困区教育为使命','中国义工大联盟','高质量志愿师资力量','自愿结成非营利性组织','民间非盈利性公益项目'],
    organization_url: ['www.cta613.org/','www.ctv521.org/','www.go9999.com/','www.chinesevolunteer.org/','www.hyy1996.org/','www.zgzyz.org.cn/','www.21tfc.org/'],


    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
